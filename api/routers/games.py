from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

router = APIRouter()


class Game(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool


class GameWithAmountWon(Game):
    total_amount_won: int | None


class Games(BaseModel):
    page_count: int
    games: list[Game]


class Message(BaseModel):
    message: str


@router.get(
    "/api/games/{game_id}",
    response_model = GameWithAmountWon,
    responses={404: {"model": Message}}
)
def get_a_game(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT
                    games.id,
                    games.episode_id,
                    games.aired,
                    games.canon,
                    sum(clues.value) AS total_amount_won
                FROM games
                LEFT OUTER JOIN clues
                    ON games.id = clues.game_id
                WHERE games.id = %s
                GROUP BY games.id

                ;
                """,
                    [game_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Game not found"}
            return {
                "id": row[0],
                "episode_id": row[1],
                "aired": row[2],
                "canon": row[3],
                "total_amount_won": row[4]
            }
